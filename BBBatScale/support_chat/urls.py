from django.urls import path
from django.views.i18n import JavaScriptCatalog

urlpatterns = [
    path('jsi18n', JavaScriptCatalog.as_view(packages=['support_chat']), name='support_chat_javascript_catalog'),
]
