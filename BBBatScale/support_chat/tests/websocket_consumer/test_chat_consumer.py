import time
from enum import Enum, auto
from typing import List

import pytest
from channels.db import database_sync_to_async
from django.contrib.auth.models import AnonymousUser, User
from django.db import transaction

from core.templatetags.user_extensions import user_get_real_name
from support_chat.models import ChatMessage
from support_chat.tests.websocket_consumer.conftest import WebsocketCommunicator, assert_connection_refuse, \
    random_message, create_chat, random_string, assert_error, fill_chat, assert_illegal_request, chat_consumer, \
    support_consumer, join_chat
from support_chat.websocket import Error


def prepare_messages(chat_communicator: WebsocketCommunicator, chat_messages: List[ChatMessage]) -> List[dict]:
    return list(map(lambda chat_message: {
        'id': chat_message.id,
        'message': chat_message.message,
        'isOwnMessage': chat_message.user == chat_communicator.user,
        'userRealName': user_get_real_name(chat_message.user),
        'timestamp': int(chat_message.timestamp.timestamp() * 1000),
    }, chat_messages))


@pytest.mark.asyncio
async def test_access_chat_without_login():
    chat_communicator_anonymous_user = chat_consumer(AnonymousUser())

    await assert_connection_refuse(chat_communicator_anonymous_user, Error.LOGIN_REQUIRED)


@pytest.mark.asyncio
async def test_access_chat_with_login(user: User):
    async with chat_consumer(user):
        pass


@pytest.mark.asyncio
async def test_access_specific_chat_without_permissions(user: User):
    chat_communicator_user = chat_consumer(user, user.username)

    await assert_connection_refuse(chat_communicator_user, Error.STAFF_REQUIRED)


@pytest.mark.asyncio
async def test_access_specific_chat_with_unknown_user(supporter: User):
    chat_communicator_staff_user = chat_consumer(supporter, 'IAmUnknown')
    await assert_connection_refuse(chat_communicator_staff_user, Error.CHAT_NOT_FOUND)


@pytest.mark.asyncio
async def test_access_specific_chat_with_user_without_chat(user: User, supporter: User):
    chat_communicator_staff_user = chat_consumer(supporter, user.username)
    await assert_connection_refuse(chat_communicator_staff_user, Error.CHAT_NOT_FOUND)


@pytest.mark.asyncio
async def test_access_specific_chat_with_permissions(user: User, supporter: User):
    async with create_chat(user):
        async with chat_consumer(supporter, user.username):
            pass


@pytest.mark.asyncio
async def test_illegal_requests_user(user: User):
    async with chat_consumer(user) as chat_communicator_user:
        await assert_illegal_request(chat_communicator_user)


@pytest.mark.asyncio
async def test_illegal_requests_supporter(user: User, supporter: User):
    async with create_chat(user):
        async with chat_consumer(supporter, user.username) as chat_communicator_supporter:
            await assert_illegal_request(chat_communicator_supporter)


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_create_chat_via_send(user: User, other_user: User, supporter: User):
    async with support_consumer(supporter) as support_communicator_supporter:
        async with chat_consumer(user) as chat_communicator_user:
            async with chat_consumer(other_user):
                await support_communicator_supporter.receive_nothing(timeout=1)

                message = random_message()
                await chat_communicator_user.send_json_to({
                    'type': 'send',
                    'message': message
                })

                response = await chat_communicator_user.receive_json_from()
                assert response['type'] == 'message'

                assert await support_communicator_supporter.receive_json_from() == {
                    'type': 'chat',
                    'chatOwner': user.username,
                    'chatOwnerRealName': user_get_real_name(user),
                    'unreadMessages': 1,
                    'isSupportActive': False,
                    'message': message,
                    'timestamp': response['timestamp']
                }

                assert await support_communicator_supporter.receive_json_from() == {
                    'type': 'newMessage',
                    'chatOwner': user.username,
                    'message': message,
                    'timestamp': response['timestamp']
                }

                assert await support_communicator_supporter.receive_json_from() == {
                    'type': 'unreadMessagesCount',
                    'chatOwner': user.username,
                    'unreadMessagesCount': 1
                }


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_send_message(user: User, other_user: User, supporter: User):
    async def assert_malformed_send(chat_communicator: WebsocketCommunicator):
        await chat_communicator.send_json_to({
            'type': 'send',
            'message': 42
        })
        await assert_error(chat_communicator, Error.MALFORMED_REQUEST)

        await chat_communicator.send_json_to({
            'type': 'send'
        })
        await assert_error(chat_communicator, Error.MALFORMED_REQUEST)

    unread_user_messages = 0
    unread_supporter_messages = 0

    class Direction(Enum):
        TO_SUPPORTER = auto()
        TO_USER = auto()

    async def assert_send(direction: Direction, message: str):
        nonlocal unread_user_messages
        nonlocal unread_supporter_messages

        if direction == Direction.TO_SUPPORTER:
            chat_communicator_sender = chat_communicator_user
            unread_user_messages += 1
        else:
            chat_communicator_sender = chat_communicator_supporter
            unread_supporter_messages += 1

        sender_real_name = user_get_real_name(chat_communicator_sender.user)

        await chat_communicator_sender.send_json_to({
            'type': 'send',
            'message': message
        })
        timestamp = int(time.time() * 1000)

        response = await chat_communicator_user.receive_json_from()
        timestamp_sender = response.pop('timestamp')
        assert isinstance(response.pop('id'), int)
        assert (timestamp - 1000) < timestamp_sender < (timestamp + 1000)
        assert response == {
            'type': 'message',
            'message': message,
            'isOwnMessage': direction == Direction.TO_SUPPORTER,
            'userRealName': sender_real_name,
        }

        response = await chat_communicator_supporter.receive_json_from()
        assert isinstance(response.pop('id'), int)
        assert timestamp_sender == response.pop('timestamp')
        assert response == {
            'type': 'message',
            'message': message,
            'isOwnMessage': direction == Direction.TO_USER,
            'userRealName': sender_real_name,
        }

        if direction == Direction.TO_USER:
            assert await chat_communicator_user.receive_json_from() == {
                'type': 'unreadMessagesCount',
                'unreadMessagesCount': unread_supporter_messages
            }

            assert await chat_communicator_supporter.receive_json_from() == {
                'type': 'unreadMessagesCount',
                'unreadMessagesCount': unread_user_messages
            }

    async with create_chat(user) as chat:
        async with chat_consumer(user) as chat_communicator_user:
            async with chat_consumer(other_user):
                async with chat_consumer(supporter, user.username) as chat_communicator_supporter:
                    await chat_communicator_supporter.send_json_to({
                        'type': 'send',
                        'message': random_message()
                    })
                    await assert_error(chat_communicator_supporter, Error.SUPPORTER_NOT_JOINED_CHAT)

                    await assert_send(Direction.TO_SUPPORTER, random_message())

                    async with join_chat(user, chat_communicator_supporter, chat_communicator_user):
                        await assert_malformed_send(chat_communicator_user)
                        await assert_malformed_send(chat_communicator_supporter)

                        await assert_send(Direction.TO_SUPPORTER, random_message())
                        await assert_send(Direction.TO_USER, random_message())

                        await assert_send(Direction.TO_SUPPORTER, random_string(250))
                        await chat_communicator_user.send_json_to({
                            'type': 'send',
                            'message': random_string(251)
                        })
                        await assert_error(chat_communicator_user, Error.MESSAGE_TOO_LONG)

                        await database_sync_to_async(chat.refresh_from_db)()
                        assert await database_sync_to_async(lambda: chat.owner)() == user

                        assert await database_sync_to_async(
                            lambda: ChatMessage.objects.filter(chat=chat).count())() == 4

                        assert chat.has_unread_owner_messages is True
                        assert chat.unread_owner_messages == 3

                        assert chat.is_support_active
                        assert chat.support_active == 1
                        assert chat.has_unread_support_messages is True
                        assert chat.unread_support_messages == 1

                        assert chat.has_unread_messages


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_history(user: User, other_user: User, supporter: User):
    async def assert_history(chat_communicator: WebsocketCommunicator):
        prepared_messages = prepare_messages(chat_communicator, chat_messages)

        await chat_communicator.send_json_to({
            'type': 'getHistory',
            'beforeMessageId': 'I am not an int!'
        })
        await assert_error(chat_communicator, Error.MALFORMED_REQUEST)

        await chat_communicator.send_json_to({
            'type': 'getHistory'
        })
        assert await chat_communicator.receive_json_from() == {
            'type': 'history',
            'messages': prepared_messages[:4:-1]
        }

        await chat_communicator.send_json_to({
            'type': 'getHistory',
            'beforeMessageId': prepared_messages[5]['id']
        })
        assert await chat_communicator.receive_json_from() == {
            'type': 'history',
            'messages': prepared_messages[4::-1]
        }

        await chat_communicator.send_json_to({
            'type': 'getHistory',
            'beforeMessageId': prepared_messages[0]['id']
        })
        assert await chat_communicator.receive_json_from() == {
            'type': 'historyEnd'
        }

    async with create_chat(user) as chat:
        async with fill_chat(chat, user, supporter, 15) as chat_messages:
            async with chat_consumer(user) as chat_communicator_user:
                async with chat_consumer(other_user):
                    async with chat_consumer(supporter, user.username) as chat_communicator_supporter:
                        await assert_history(chat_communicator_user)
                        await assert_history(chat_communicator_supporter)


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_new_messages(user: User, other_user: User, supporter: User):
    async def assert_new_messages(chat_communicator: WebsocketCommunicator):
        prepared_messages = prepare_messages(chat_communicator, chat_messages)

        await chat_communicator.send_json_to({
            'type': 'getNewMessages'
        })
        await assert_error(chat_communicator, Error.MALFORMED_REQUEST)

        await chat_communicator.send_json_to({
            'type': 'getNewMessages',
            'afterMessageId': 'I am not an int!'
        })
        await assert_error(chat_communicator, Error.MALFORMED_REQUEST)

        await chat_communicator.send_json_to({
            'type': 'getNewMessages',
            'afterMessageId': chat_messages[9].id
        })
        assert await chat_communicator.receive_json_from() == {
            'type': 'newMessages',
            'messages': prepared_messages[10:]
        }

        await chat_communicator.send_json_to({
            'type': 'getNewMessages',
            'afterMessageId': chat_messages[14].id
        })
        assert await chat_communicator.receive_json_from() == {
            'type': 'newMessages',
            'messages': []
        }

        await chat_communicator.send_json_to({
            'type': 'getNewMessages',
            'afterMessageId': chat_messages[4].id
        })
        assert await chat_communicator.receive_json_from() == {
            'type': 'newMessages',
            'messages': prepared_messages[5:]
        }

        await chat_communicator.send_json_to({
            'type': 'getNewMessages',
            'afterMessageId': chat_messages[3].id
        })
        assert await chat_communicator.receive_json_from() == {
            'type': 'tooManyNewMessages'
        }

        await chat_communicator.send_json_to({
            'type': 'getNewMessages',
            'afterMessageId': chat_messages[0].id
        })
        assert await chat_communicator.receive_json_from() == {
            'type': 'tooManyNewMessages'
        }

    async with create_chat(user) as chat:
        async with fill_chat(chat, user, supporter, 15) as chat_messages:
            async with chat_consumer(user) as chat_communicator_user:
                async with chat_consumer(other_user):
                    async with chat_consumer(supporter, user.username) as chat_communicator_supporter:
                        await assert_new_messages(chat_communicator_user)
                        await assert_new_messages(chat_communicator_supporter)


@pytest.mark.asyncio
@pytest.mark.django_db
async def test_messages_read(user: User, other_user: User, supporter: User):
    async with create_chat(user) as chat:
        async with chat_consumer(user) as chat_communicator_user:
            async with chat_consumer(supporter, user.username) as chat_communicator_supporter:
                async with support_consumer(supporter) as support_communicator_supporter:
                    async with chat_consumer(other_user):
                        await chat_communicator_supporter.send_json_to({
                            'type': 'messagesRead'
                        })
                        await assert_error(chat_communicator_supporter, Error.SUPPORTER_NOT_JOINED_CHAT)

                        async with join_chat(user, chat_communicator_supporter, chat_communicator_user,
                                             support_communicator_supporter):
                            async with fill_chat(chat, user, supporter, (10, 5)):
                                assert chat.unread_owner_messages == 10
                                await chat_communicator_supporter.send_json_to({
                                    'type': 'messagesRead'
                                })
                                assert await support_communicator_supporter.receive_json_from() == {
                                    'type': 'unreadMessagesCount',
                                    'chatOwner': user.username,
                                    'unreadMessagesCount': 0
                                }
                                await database_sync_to_async(chat.refresh_from_db)()
                                assert chat.unread_owner_messages == 0

                            @transaction.atomic
                            def reset_unread_messages():
                                chat.refresh_from_db()
                                chat.unread_owner_messages = 0
                                chat.unread_support_messages = 0
                                chat.save()

                            await database_sync_to_async(reset_unread_messages)()

                            async with chat_consumer(user) as other_chat_communicator_user:
                                async with fill_chat(chat, user, supporter, (5, 10)):
                                    assert chat.unread_support_messages == 10
                                    await chat_communicator_user.send_json_to({
                                        'type': 'messagesRead'
                                    })
                                    expected_user_response = {
                                        'type': 'unreadMessagesCount',
                                        'unreadMessagesCount': 0
                                    }
                                    assert await chat_communicator_user.receive_json_from() == expected_user_response
                                    assert await other_chat_communicator_user.receive_json_from() \
                                           == expected_user_response
                                    assert await chat_communicator_supporter.receive_json_from() == {
                                        'type': 'unreadMessagesCount',
                                        'unreadMessagesCount': 5
                                    }
                                    await database_sync_to_async(chat.refresh_from_db)()
                                    assert chat.unread_support_messages == 0


@pytest.mark.asyncio
async def test_get_chat_status(user: User, other_user: User, supporter: User):
    async def send_get_chat_status(chat_communicator: WebsocketCommunicator) -> bool:
        await chat_communicator.send_json_to({
            'type': 'getChatStatus'
        })

        response = await chat_communicator.receive_json_from()
        response_type = response.pop('type')
        assert response_type in ['chatGainedSupport', 'chatLostSupport']
        assert len(response) == 0

        return response_type == 'chatGainedSupport'

    async with create_chat(user):
        async with chat_consumer(user) as chat_communicator_user:
            async with support_consumer(supporter) as support_communicator_supporter:
                async with chat_consumer(other_user):
                    assert await send_get_chat_status(chat_communicator_user) is False

                    async with chat_consumer(supporter, user.username) as chat_communicator_supporter:
                        async with join_chat(user, chat_communicator_supporter, chat_communicator_user,
                                             support_communicator_supporter):
                            assert await send_get_chat_status(chat_communicator_user) is True
                            assert await send_get_chat_status(chat_communicator_supporter) is True

                    assert await send_get_chat_status(chat_communicator_user) is False


@pytest.mark.asyncio
async def test_join_leave_chat(user: User, other_user: User, staff_user: User, supporter: User):
    async def assert_gained_support(support_communicator: WebsocketCommunicator,
                                    *chat_communicators: WebsocketCommunicator):
        assert await support_communicator.receive_json_from() == {
            'type': 'chatGainedSupport',
            'chatOwner': user.username
        }

        for chat_communicator in chat_communicators:
            assert await chat_communicator.receive_json_from() == {
                'type': 'chatGainedSupport'
            }

    async def assert_lost_support(support_communicator: WebsocketCommunicator,
                                  *chat_communicators: WebsocketCommunicator):
        assert await support_communicator.receive_json_from() == {
            'type': 'chatLostSupport',
            'chatOwner': user.username
        }

        for chat_communicator in chat_communicators:
            assert await chat_communicator.receive_json_from() == {
                'type': 'chatLostSupport'
            }

    async with create_chat(user):
        async with chat_consumer(user) as chat_communicator_user:
            async with support_consumer(supporter) as support_communicator_supporter:
                async with chat_consumer(other_user):
                    await chat_communicator_user.send_json_to({
                        'type': 'joinChat'
                    })
                    await assert_error(chat_communicator_user, Error.PERMISSION_DENIED)

                    async with chat_consumer(supporter, user.username) as chat_communicator_supporter:
                        await chat_communicator_supporter.send_json_to({
                            'type': 'joinChat'
                        })

                        await assert_gained_support(support_communicator_supporter, chat_communicator_user,
                                                    chat_communicator_supporter)

                        await chat_communicator_supporter.send_json_to({
                            'type': 'joinChat'
                        })

                    await assert_lost_support(support_communicator_supporter, chat_communicator_user)

                    async with chat_consumer(supporter, user.username) as chat_communicator_supporter:
                        async with join_chat(user, chat_communicator_supporter, chat_communicator_user,
                                             support_communicator_supporter):
                            await chat_communicator_supporter.send_json_to({
                                'type': 'joinChat'
                            })

                        await chat_communicator_supporter.send_json_to({
                            'type': 'leaveChat'
                        })
                        await chat_communicator_supporter.receive_nothing(timeout=1)

                    async with chat_consumer(supporter, user.username) as chat_communicator_supporter:
                        async with join_chat(user, chat_communicator_supporter, chat_communicator_user,
                                             support_communicator_supporter):
                            await support_communicator_supporter.send_json_to({
                                'type': 'setStatus',
                                'status': 'inactive'
                            })
                            assert await support_communicator_supporter.receive_json_from() == {
                                'type': 'status',
                                'status': 'inactive'
                            }
                            assert await chat_communicator_user.receive_json_from() == {
                                'type': 'supportOffline'
                            }
                            assert await chat_communicator_supporter.receive_json_from() == {
                                'type': 'supportOffline'
                            }

                            await assert_lost_support(support_communicator_supporter, chat_communicator_user,
                                                      chat_communicator_supporter)

                            await support_communicator_supporter.send_json_to({
                                'type': 'setStatus',
                                'status': 'active'
                            })
                            assert await support_communicator_supporter.receive_json_from() == {
                                'type': 'status',
                                'status': 'active'
                            }
                            assert await chat_communicator_user.receive_json_from() == {
                                'type': 'supportOnline'
                            }
                            assert await chat_communicator_supporter.receive_json_from() == {
                                'type': 'supportOnline'
                            }

                            await support_communicator_supporter.receive_nothing(timeout=1)
                            await chat_communicator_user.receive_nothing(timeout=1)
                            await chat_communicator_supporter.receive_nothing(timeout=1)

                            await chat_communicator_supporter.send_json_to({
                                'type': 'joinChat'
                            })

                            await assert_gained_support(support_communicator_supporter, chat_communicator_user,
                                                        chat_communicator_supporter)

                    async with chat_consumer(staff_user, user.username) as chat_communicator_staff_user:
                        await chat_communicator_staff_user.send_json_to({
                            'type': 'joinChat'
                        })
                        await assert_error(chat_communicator_staff_user, Error.SUPPORTER_STATUS_INACTIVE)

                        await chat_communicator_staff_user.send_json_to({
                            'type': 'leaveChat'
                        })
                        await assert_error(chat_communicator_staff_user, Error.SUPPORTER_STATUS_INACTIVE)


@pytest.mark.asyncio
async def test_get_support_status(user: User, other_user: User, staff_user: User):
    async def send_get_support_status(chat_communicator: WebsocketCommunicator) -> bool:
        await chat_communicator.send_json_to({
            'type': 'getSupportStatus'
        })

        response = await chat_communicator.receive_json_from()
        response_type = response.pop('type')
        assert response_type in ['supportOnline', 'supportOffline']
        assert len(response) == 0

        return response_type == 'supportOnline'

    async with chat_consumer(user) as chat_communicator_user:
        async with support_consumer(staff_user) as support_communicator_staff_user:
            async with chat_consumer(other_user):
                assert await send_get_support_status(chat_communicator_user) is False

                await support_communicator_staff_user.send_json_to({
                    'type': 'setStatus',
                    'status': 'active'
                })

                assert await chat_communicator_user.receive_json_from() == {
                    'type': 'supportOnline'
                }
                assert await send_get_support_status(chat_communicator_user) is True

                await support_communicator_staff_user.send_json_to({
                    'type': 'setStatus',
                    'status': 'inactive'
                })

                assert await chat_communicator_user.receive_json_from() == {
                    'type': 'supportOffline'
                }
                assert await send_get_support_status(chat_communicator_user) is False


@pytest.mark.asyncio
async def test_get_unread_messages_count(user: User, other_user: User, supporter: User):
    async def send_get_unread_messages_count(
            chat_communicator: WebsocketCommunicator) -> int:
        await chat_communicator.send_json_to({
            'type': 'getUnreadMessagesCount'
        })
        unread_messages_count_response = await chat_communicator.receive_json_from()
        unread_messages_count = unread_messages_count_response.pop('unreadMessagesCount')

        assert unread_messages_count_response == {
            'type': 'unreadMessagesCount'
        }
        assert isinstance(unread_messages_count, int)
        return unread_messages_count

    async with create_chat(user) as chat:
        async with fill_chat(chat, user, supporter):
            async with chat_consumer(user) as chat_communicator_user:
                async with chat_consumer(supporter, user.username) as chat_communicator_supporter:
                    async with chat_consumer(user):
                        async with chat_consumer(other_user):
                            assert chat.unread_support_messages == await send_get_unread_messages_count(
                                chat_communicator_user)

                            assert chat.unread_owner_messages == await send_get_unread_messages_count(
                                chat_communicator_supporter)
