from __future__ import annotations

import random
import string
from contextlib import asynccontextmanager
from typing import Union, List, Optional, Tuple

import pytest
from channels.db import database_sync_to_async
from channels.testing import WebsocketCommunicator as ChannelsWebsocketCommunicator
from django.contrib.auth.models import AnonymousUser
from core.models import User
from django.db import transaction

from BBBatScale.routing import websocket_urlrouter
from support_chat.models import Chat, ChatMessage, Supporter
from support_chat.websocket import WebsocketConsumerNotOpenError, Error


def random_string(length: int) -> str:
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for _ in range(length))


def random_message() -> str:
    return random_string(random.randint(16, 128))


@pytest.mark.asyncio
@pytest.fixture(scope='function')
async def user(db) -> User:
    user = await database_sync_to_async(
        lambda: User.objects.create(username='john_doe', first_name='John', last_name='Doe'))()
    yield user
    await database_sync_to_async(user.delete)()


@pytest.mark.asyncio
@pytest.fixture(scope='function')
async def other_user(db) -> User:
    user = await database_sync_to_async(
        lambda: User.objects.create(username='jane_doe', first_name='Jane', last_name='Doe'))()
    yield user
    await database_sync_to_async(user.delete)()


@pytest.mark.asyncio
@pytest.fixture(scope='function')
async def staff_user(db) -> User:
    user = await database_sync_to_async(
        lambda: User.objects.create(username='john_roe', first_name='John', last_name='Roe', is_staff=True))()
    yield user
    await database_sync_to_async(user.delete)()


@asynccontextmanager
async def create_supporter() -> User:
    @transaction.atomic
    def create() -> Tuple[User, Supporter]:
        _user = User.objects.create(username='jane_roe', first_name='Jane', last_name='Roe', is_staff=True)
        _supporter = Supporter.get_or_create(user=_user)
        _supporter.is_active = True
        _supporter.save()
        return _user, _supporter

    @transaction.atomic
    def delete(_user: User, _supporter: Supporter):
        _supporter.delete()
        _user.delete()

    user, supporter = await database_sync_to_async(create)()

    yield user

    await database_sync_to_async(delete)(user, supporter)


@pytest.mark.asyncio
@pytest.fixture(scope='function')
async def supporter(db) -> User:
    async with create_supporter() as supporter:
        yield supporter


class WebsocketCommunicator(ChannelsWebsocketCommunicator):

    def __init__(self, path: str, user: Union[User, AnonymousUser]):
        super().__init__(websocket_urlrouter, path)
        self.user = user
        self.scope['user'] = user

    async def __aenter__(self) -> WebsocketCommunicator:
        connected, _ = await self.connect()
        assert connected is True
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb) -> bool:
        receive_nothing_error = None
        try:
            assert await self.receive_nothing(timeout=1)
        except BaseException as receive_nothing_error:
            receive_nothing_error.__cause__ = exc_val

        try:
            await self.disconnect()
        except BaseException as e:
            if receive_nothing_error:
                e.__cause__ = receive_nothing_error
            else:
                e.__cause__ = exc_val
            raise e
        return False

    async def get_close_code(self, timeout: int = 1) -> int:
        response = await self.receive_output(timeout)
        assert response['type'] == 'websocket.close'
        assert list(response.keys()) == ['type', 'code']

        code = response['code']
        assert isinstance(code, int)
        return code


def chat_consumer(user: Union[User, AnonymousUser], chat_owner: Optional[str] = None) -> WebsocketCommunicator:
    if chat_owner is None:
        return WebsocketCommunicator('/ws/supportchat/chat', user)
    else:
        return WebsocketCommunicator(f'/ws/supportchat/chat/{chat_owner}', user)


def support_consumer(user: Union[User, AnonymousUser]) -> WebsocketCommunicator:
    return WebsocketCommunicator('/ws/supportchat/support', user)


@asynccontextmanager
async def create_chat(owner: User) -> Chat:
    chat, created = await database_sync_to_async(Chat.get_or_create)(owner=owner)
    assert created

    yield chat

    await database_sync_to_async(chat.delete)()


@asynccontextmanager
async def fill_chat(chat: Chat, owner: User, supporter: User,
                    message_count: Optional[Union[int, Tuple[int, int]]] = None) -> List[ChatMessage]:
    @transaction.atomic
    def create() -> List[ChatMessage]:
        chat.refresh_from_db()
        unread_messages = chat.unread_owner_messages + chat.unread_support_messages

        if message_count is None or isinstance(message_count, int):
            _messages = list(
                ChatMessage(user=random.choice([owner, supporter]), chat=chat, message=random_message())
                for _ in range(message_count or random.randint(4, 8))
            )
        else:
            owner_message_count, supporter_message_count = message_count
            owner_messages = list(
                ChatMessage(user=owner, chat=chat, message=random_message())
                for _ in range(owner_message_count)
            )
            supporter_messages = list(
                ChatMessage(user=supporter, chat=chat, message=random_message())
                for _ in range(supporter_message_count)
            )

            _messages = owner_messages + supporter_messages

        chat.refresh_from_db()
        assert chat.unread_owner_messages + chat.unread_support_messages == unread_messages

        for message in _messages:
            message.save()

        chat.refresh_from_db()
        assert chat.unread_owner_messages + chat.unread_support_messages == unread_messages + len(_messages)
        return _messages

    @transaction.atomic
    def delete(_messages):
        for message in _messages:
            message.delete()

    messages = await database_sync_to_async(create)()

    yield messages

    await database_sync_to_async(delete)(messages)


@asynccontextmanager
async def join_chat(
        user: User, chat_communicator_supporter: WebsocketCommunicator,
        chat_communicators: Union[None, WebsocketCommunicator, List[WebsocketCommunicator]] = None,
        support_communicators: Union[None, WebsocketCommunicator, List[WebsocketCommunicator]] = None) -> None:
    if chat_communicators is None:
        chat_communicators = []
    elif isinstance(chat_communicators, WebsocketCommunicator):
        chat_communicators = [chat_communicators]

    if support_communicators is None:
        support_communicators = []
    elif isinstance(support_communicators, WebsocketCommunicator):
        support_communicators = [support_communicators]

    await chat_communicator_supporter.send_json_to({
        'type': 'joinChat'
    })

    response: dict = await chat_communicator_supporter.receive_json_from()
    assert response == {
        'type': 'chatGainedSupport'
    }
    for chat_communicator in chat_communicators:
        response: dict = await chat_communicator.receive_json_from()
        assert response == {
            'type': 'chatGainedSupport'
        }
    for support_communicator in support_communicators:
        response: dict = await support_communicator.receive_json_from()
        assert response == {
            'type': 'chatGainedSupport',
            'chatOwner': user.username
        }

    yield

    await chat_communicator_supporter.send_json_to({
        'type': 'leaveChat'
    })

    response: dict = await chat_communicator_supporter.receive_json_from()
    assert response == {
        'type': 'chatLostSupport'
    }
    for chat_communicator in chat_communicators:
        response: dict = await chat_communicator.receive_json_from()
        assert response == {
            'type': 'chatLostSupport'
        }
    for support_communicator in support_communicators:
        response: dict = await support_communicator.receive_json_from()
        assert response == {
            'type': 'chatLostSupport',
            'chatOwner': user.username
        }


async def assert_error(communicator: WebsocketCommunicator, error: Error):
    response = await communicator.receive_json_from()
    assert 'type' in response
    assert response['type'] == 'error'
    assert Error(response['code']) == error


async def assert_connection_refuse(communicator: WebsocketCommunicator, error: Error):
    with pytest.raises(WebsocketConsumerNotOpenError) as exc_info:
        async with communicator:
            await assert_error(communicator, error)
            assert await communicator.get_close_code() == 1000

    if exc_info.value.__cause__ is not None:
        raise exc_info.value.__cause__


async def assert_illegal_request(communicator: WebsocketCommunicator):
    await communicator.send_json_to({})
    await assert_error(communicator, Error.MALFORMED_REQUEST)

    await communicator.send_json_to({
        'type': 'I am an illegal request!'
    })
    await assert_error(communicator, Error.UNKNOWN_REQUEST)
