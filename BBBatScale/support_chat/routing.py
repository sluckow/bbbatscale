from django.urls import path

from support_chat.websocket import ChatConsumer, SupportConsumer

websocket_urlpatterns = [
    path('chat', ChatConsumer),
    path('chat/<str:username>', ChatConsumer),

    path('support', SupportConsumer),
]
