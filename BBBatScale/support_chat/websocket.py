from __future__ import annotations

import json
from asyncio import iscoroutinefunction
from datetime import datetime
from enum import Enum, unique, auto
from functools import wraps
from json import JSONEncoder
from types import MethodType, FunctionType
from typing import Type, Union, Callable, Coroutine, Any, Optional, List, Tuple

from channels.db import database_sync_to_async
from channels.generic.websocket import AsyncJsonWebsocketConsumer
from django.db import transaction

from core.models import User
from core.templatetags.user_extensions import user_get_real_name
from support_chat.models import ChatMessage, Chat, Supporter


class WebsocketConsumerNotOpenError(RuntimeError):
    def __init__(self):
        super().__init__('The WebSocket is not open')


class WebsocketConsumerAlreadyAcceptedError(RuntimeError):
    def __init__(self):
        super().__init__('The WebSocket has already been accepted')


class WebsocketConsumerNotAcceptedError(RuntimeError):
    def __init__(self):
        super().__init__('The WebSocket has not been accepted yet')


class WebsocketConsumerAlreadyClosedError(RuntimeError):
    def __init__(self):
        super().__init__('The WebSocket has already been closed')


class WebsocketConsumerError(RuntimeError):
    def __init__(self, error: Error):
        super().__init__(error.message)
        self.error = error


@unique
class Error(Enum):

    def __init__(self, code, message):
        self._value_ = code
        self._message_ = message

    @property
    def code(self) -> int:
        return self.value

    @property
    def message(self) -> str:
        return self._message_

    def to_dict(self):
        return dict(type='error', code=self.code, message=self.message)

    def __repr__(self):
        return f"{self.__class__.__name__}.{self.name}"

    def __str__(self):
        return f"{self.__class__.__name__}.{self.name}: {self.message}"

    def __eq__(self, other):
        if isinstance(other, int):
            return self.code == other
        return super().__eq__(other)

    @classmethod
    def _missing_(cls, value):
        for error in cls.__members__.values():
            if error == value:
                return error
        return None

    INTERNAL_ERROR = (1, 'An unexpected internal error occurred.')
    LOGIN_REQUIRED = (2, 'Login required.')
    STAFF_REQUIRED = (3, 'You have to be staff to access this content.')
    SUPPORTER_STATUS_INACTIVE = (4, 'Your support status is inactive. Set it to active to access this feature.')
    CHAT_NOT_FOUND = (5, 'The chat could not be found.')
    MESSAGE_TOO_LONG = (6, 'The message is too long.')
    MALFORMED_REQUEST = (7, 'The request is malformed.')
    UNKNOWN_REQUEST = (8, 'The request is unknown.')
    PERMISSION_DENIED = (9, 'The request is not allowed in this context.')
    SUPPORTER_NOT_JOINED_CHAT = (10, 'You have not joined the chat yet.')


class ExtendedJsonEncoder(JSONEncoder):
    def default(self, obj: Any) -> Any:
        if isinstance(obj, Error):
            return obj.to_dict()

        return super().default(obj)


@unique
class ChatUserType(Enum):
    OWNER = auto()
    SUPPORTER = auto()


def websocket_consumer_decorator(clazz: Type[WebsocketConsumer]) -> Callable[[dict], WebsocketConsumer]:
    @wraps(clazz)
    def wrapper(scope: dict):
        user = scope['user']
        args = scope['url_route']['args']
        kwargs = scope['url_route']['kwargs']

        return clazz(scope=scope, user=user, *args, **kwargs)

    return wrapper


class WebsocketConsumer(AsyncJsonWebsocketConsumer):

    def __init__(self, scope: dict, user: User, *args: list, **kwargs: dict):
        super().__init__(scope=scope)
        self.user = user
        self._accepted = False
        self._closed = False

    async def group_send_json(self, group: str,
                              handler: Union[Callable[[Any], Coroutine[Any, Any, None]], str],
                              *args, **kwargs):
        if (isinstance(handler, MethodType) or isinstance(handler, FunctionType)) and iscoroutinefunction(handler):
            handler = handler.__name__
        elif not isinstance(handler, str):
            raise ValueError('\'handler\' is neither a string nor a async method/function')

        await self.channel_layer.group_send(group, {
            'type': 'send_json_from_group',
            'handler': handler,
            'args': args,
            'kwargs': kwargs
        })

    async def send_json_from_group(self, event: dict):
        await getattr(self, event['handler'])(*event['args'], **event['kwargs'])

    async def accept(self, subprotocol=None):
        if self._accepted:
            raise WebsocketConsumerAlreadyAcceptedError
        self._accepted = True
        await super().accept(subprotocol)

    @property
    def accepted(self) -> bool:
        return self._accepted

    async def close(self, code=None):
        if not self._accepted:
            raise WebsocketConsumerNotAcceptedError
        if self._closed:
            raise WebsocketConsumerAlreadyClosedError
        self._closed = True
        await super().close(code)

    @property
    def closed(self) -> bool:
        return self._closed

    @property
    def opened(self) -> bool:
        return self.accepted and not self.closed

    @classmethod
    async def encode_json(cls, content):
        return json.dumps(content, cls=ExtendedJsonEncoder)

    @classmethod
    def if_opened(cls, method: MethodType = None, raise_exception: bool = False):
        def decorator(_method):
            @wraps(_method)
            async def wrapper(self: WebsocketConsumer, *args, **kwargs):
                if self.opened:
                    return await _method(self, *args, **kwargs)
                if raise_exception:
                    raise WebsocketConsumerNotOpenError

            return wrapper

        if method:
            return decorator(method)
        return decorator

    @classmethod
    def passes_test(cls, test: Callable[[WebsocketConsumer], Coroutine[Any, Any, Optional[Error]]],
                    accept: bool = False, close: bool = False, raise_exception: bool = False):
        def decorator(method):
            @wraps(method)
            async def wrapper(self: WebsocketConsumer, *args, **kwargs):
                if accept and not self.accepted:
                    await self.accept()
                error = await test(self)

                if not error:
                    return await method(self, *args, **kwargs)
                else:
                    await self.send_json(error)
                    if close:
                        await self.close(1000)
                    if raise_exception:
                        raise WebsocketConsumerError(error)

            return wrapper

        return decorator

    @classmethod
    def login_required(cls, method: MethodType = None, accept: bool = False, close: bool = False,
                       raise_exception: bool = False):
        async def test(self: WebsocketConsumer) -> Optional[Error]:
            is_authenticated = self.user.is_active and self.user.is_authenticated
            return None if is_authenticated else Error.LOGIN_REQUIRED

        decorator = cls.passes_test(test, accept=accept, close=close, raise_exception=raise_exception)
        if method:
            return decorator(method)
        return decorator

    @classmethod
    def staff_required(cls, method: MethodType = None, accept: bool = False, close: bool = False,
                       raise_exception: bool = False):
        async def test(self: WebsocketConsumer) -> Optional[Error]:
            is_staff = self.user.is_active and self.user.is_authenticated and (
                        self.user.is_staff or self.user.is_superuser)
            return None if is_staff else Error.STAFF_REQUIRED

        decorator = cls.passes_test(test, accept=accept, close=close, raise_exception=raise_exception)
        if method:
            return decorator(method)
        return decorator


class SupportStatusListener:
    GROUP = 'support_status'

    async def support_status_changed(self, support_online: bool):
        pass


class SupporterStatusListener:
    GROUP = 'supporter_status'

    async def supporter_status_changed(self, is_active: bool, username: str):
        pass


@websocket_consumer_decorator
class ChatConsumer(SupportStatusListener, SupporterStatusListener, WebsocketConsumer):

    def __init__(self, scope: dict, user: User, username: str = None):
        super().__init__(scope, user)
        if username:
            self.is_supporter_joined = False
            self.chat_owner_username = username
        else:
            self.is_supporter_joined = None
            self.chat_owner_username = None  # user.username is not ready for use at this point

        # can not add self.chat_group to self.groups yet, since self.chat_owner_username may has not been initialized
        self.groups = [SupportStatusListener.GROUP, SupporterStatusListener.GROUP]

    @property
    def is_supporter(self) -> bool:
        return isinstance(self.is_supporter_joined, bool)

    @property
    def group(self) -> str:
        if self.chat_owner_username is None:
            raise ValueError('self.chat_owner_username has not been set yet')
        return f'chat_{self.chat_owner_username}'

    async def test_permissions(self) -> Optional[Error]:
        if self.is_supporter and not (
                self.user.is_active and self.user.is_authenticated and (self.user.is_staff or self.user.is_superuser)):
            return Error.STAFF_REQUIRED
        return None

    async def test_chat_exists(self) -> Optional[Error]:
        if self.is_supporter and not await database_sync_to_async(
                lambda: Chat.objects.filter(id__username=self.chat_owner_username).exists())():
            return Error.CHAT_NOT_FOUND
        return None

    async def test_is_active_supporter(self) -> Optional[Error]:
        if not self.is_supporter:
            return Error.PERMISSION_DENIED
        else:
            supporter = await database_sync_to_async(Supporter.get)(user=self.user)
            if not supporter or not supporter.is_active:
                return Error.SUPPORTER_STATUS_INACTIVE
        return None

    async def test_is_user_or_supporter_joined(self) -> Optional[Error]:
        if self.is_supporter_joined is False:
            return Error.SUPPORTER_NOT_JOINED_CHAT
        return None

    @WebsocketConsumer.login_required(accept=True, close=True)
    @WebsocketConsumer.passes_test(lambda self: self.test_permissions(), close=True)
    @WebsocketConsumer.passes_test(lambda self: self.test_chat_exists(), close=True)
    async def connect(self):
        if not self.is_supporter:
            self.chat_owner_username = self.user.username

        self.groups.append(self.group)
        await self.channel_layer.group_add(self.group, self.channel_name)
        # add self.chat_group to self.groups and call group_add, since self.chat_owner_username is now initialized

    @WebsocketConsumer.if_opened(raise_exception=True)
    async def receive_json(self, content: dict, **kwargs):  # noqa: C901
        if 'type' not in content:
            await self.send_json(Error.MALFORMED_REQUEST)
            return

        content_type = content['type']
        if content_type == 'getHistory':
            if 'beforeMessageId' in content and not isinstance(content['beforeMessageId'], int):
                await self.send_json(Error.MALFORMED_REQUEST)
                return
            await self.command_get_history(content.get('beforeMessageId', None))
        elif content_type == 'getNewMessages':
            if 'afterMessageId' not in content or not isinstance(content['afterMessageId'], int):
                await self.send_json(Error.MALFORMED_REQUEST)
                return
            await self.command_get_new_messages(content['afterMessageId'])
        elif content_type == 'send':
            if 'message' not in content or not isinstance(content['message'], str):
                await self.send_json(Error.MALFORMED_REQUEST)
                return
            await self.command_send(content['message'])
        elif content_type == 'messagesRead':
            await self.command_messages_read()
        elif content_type == 'getUnreadMessagesCount':
            await self.command_get_unread_messages_count()
        elif content_type == 'getChatStatus':
            await self.command_get_chat_status()
        elif content_type == 'getSupportStatus':
            await self.command_get_support_status()
        elif content_type == 'joinChat':
            await self.command_join_chat()
        elif content_type == 'leaveChat':
            await self.command_leave_chat()
        else:
            await self.send_json(Error.UNKNOWN_REQUEST)

    async def command_get_history(self, before_message_id: Optional[int]):
        chat = await database_sync_to_async(Chat.get)(owner=self.chat_owner_username)
        if chat is not None:
            @transaction.atomic
            def get_history() -> List[dict]:
                if isinstance(before_message_id, int):
                    messages_query = ChatMessage.objects.filter(chat=chat, id__lt=before_message_id)
                else:
                    messages_query = ChatMessage.objects.filter(chat=chat)
                messages_query = messages_query.order_by('-id')
                return list(
                    map(lambda chat_message: self._inflate_chat_message_dict(_chat_message_to_dict(chat_message)),
                        messages_query[:10]))

            messages = await database_sync_to_async(get_history)()
            if messages:
                await self.send_json({
                    'type': 'history',
                    'messages': messages
                })
            else:
                await self.send_json({
                    'type': 'historyEnd',
                })
        else:
            await self.send_json({
                'type': 'historyEnd',
            })

    async def command_get_new_messages(self, after_message_id: int):
        chat = await database_sync_to_async(Chat.get)(owner=self.chat_owner_username)
        if chat is not None:
            @transaction.atomic
            def get_messages() -> Optional[List[dict]]:
                messages_query = ChatMessage.objects.filter(chat=chat, id__gt=after_message_id).order_by('id')
                if messages_query.count() > 10:
                    return None
                return list(
                    map(lambda chat_message: self._inflate_chat_message_dict(_chat_message_to_dict(chat_message)),
                        messages_query))

            messages = await database_sync_to_async(get_messages)()
            if messages is not None:
                await self.send_json({
                    'type': 'newMessages',
                    'messages': messages
                })
            else:
                await self.send_json({
                    'type': 'tooManyNewMessages'
                })
        else:
            await self.send_json({
                'type': 'newMessages',
                'messages': []
            })

    @WebsocketConsumer.passes_test(lambda self: self.test_is_user_or_supporter_joined())
    async def command_send(self, message: str):
        if len(message.encode('utf-16-le')) / 2 > 250:
            await self.send_json(Error.MESSAGE_TOO_LONG)
            return

        @transaction.atomic
        def create_chat_message() -> Tuple[Chat, bool, User, ChatMessage]:
            _chat, _created = Chat.get_or_create(owner=self.chat_owner_username)
            _chat_message = ChatMessage(chat=_chat, user=self.user, message=message)
            _chat_message.save(is_supporter=self.is_supporter)
            return _chat, _created, _chat.owner, _chat_message

        chat, created, owner, chat_message = await database_sync_to_async(create_chat_message)()

        if created:
            await self.group_send_json(SupportConsumer.GROUP, SupportConsumer.send_chat, owner.username,
                                       user_get_real_name(owner), chat.unread_owner_messages,
                                       chat.is_support_active, chat_message.message,
                                       _datetime_to_epoch_millis(chat_message.timestamp))

        await self.group_send_json(self.group, self.send_chat_message_dict,
                                   chat_message=_chat_message_to_dict(chat_message))
        await self.group_send_json(SupportConsumer.GROUP, SupportConsumer.new_message,
                                   chat_owner=self.chat_owner_username, message=chat_message.message,
                                   timestamp=_datetime_to_epoch_millis(chat_message.timestamp))

        if self.is_supporter:
            await self.group_send_json(self.group, self.unread_messages_count)
        else:
            await self.group_send_json(SupportConsumer.GROUP, SupportConsumer.unread_messages_count,
                                       chat_owner=self.chat_owner_username)

    @WebsocketConsumer.passes_test(lambda self: self.test_is_user_or_supporter_joined())
    async def command_messages_read(self):
        @transaction.atomic
        def messages_read() -> bool:
            chat = Chat.get(owner=self.chat_owner_username)
            if chat is not None:
                if self.is_supporter and chat.unread_owner_messages != 0:
                    chat.unread_owner_messages = 0
                elif not self.is_supporter and chat.unread_support_messages != 0:
                    chat.unread_support_messages = 0
                else:
                    return False
                chat.save()
                return True
            return False

        if await database_sync_to_async(messages_read)():
            if self.is_supporter:
                await self.group_send_json(SupportConsumer.GROUP, SupportConsumer.unread_messages_count,
                                           chat_owner=self.chat_owner_username)
            else:
                await self.group_send_json(self.group, self.unread_messages_count)

    async def command_get_unread_messages_count(self):
        await self.unread_messages_count()

    async def command_get_chat_status(self):
        chat = await database_sync_to_async(Chat.get)(owner=self.chat_owner_username)
        if chat is not None:
            if chat.is_support_active:
                await self.chat_gained_support()
            else:
                await self.chat_lost_support()

    async def command_get_support_status(self):
        await self.support_status_changed(await database_sync_to_async(Supporter.has_active)())

    @WebsocketConsumer.passes_test(lambda self: self.test_is_active_supporter())
    async def command_join_chat(self):
        await self._supporter_joined_chat()

    @WebsocketConsumer.passes_test(lambda self: self.test_is_active_supporter())
    async def command_leave_chat(self):
        await self._supporter_left_chat()

    async def support_status_changed(self, support_online: bool):
        if support_online:
            await self.send_json({
                'type': 'supportOnline'
            })
        else:
            await self.send_json({
                'type': 'supportOffline'
            })

    async def supporter_status_changed(self, is_active: bool, username: str):
        if not is_active:
            await self._supporter_left_chat()

    async def unread_messages_count(self):
        unread_messages = 0
        chat = await database_sync_to_async(Chat.get)(owner=self.chat_owner_username)
        if chat is not None:
            if self.is_supporter:
                unread_messages = chat.unread_owner_messages
            else:
                unread_messages = chat.unread_support_messages

        await self.send_json({
            'type': 'unreadMessagesCount',
            'unreadMessagesCount': unread_messages
        })

    async def chat_gained_support(self):
        await self.send_json({
            'type': 'chatGainedSupport'
        })

    async def chat_lost_support(self):
        await self.send_json({
            'type': 'chatLostSupport'
        })

    @WebsocketConsumer.if_opened(raise_exception=True)
    async def disconnect(self, code: int):
        if self.is_supporter:
            await self._supporter_left_chat()

    async def _supporter_joined_chat(self):
        if self.is_supporter_joined is False:
            self.is_supporter_joined = True

            @transaction.atomic
            def supporter_join_chat() -> bool:
                chat = Chat.get(owner=self.chat_owner_username)
                is_first_supporter = not chat.is_support_active
                chat.increment_support_active()
                chat.save()
                return is_first_supporter

            if await database_sync_to_async(supporter_join_chat)():
                await self.group_send_json(self.group, self.chat_gained_support)
                await self.group_send_json(SupportConsumer.GROUP, SupportConsumer.chat_gained_support,
                                           self.chat_owner_username)

    async def _supporter_left_chat(self):
        if self.is_supporter_joined is True:
            self.is_supporter_joined = False

            @transaction.atomic
            def supporter_leave_chat() -> bool:
                chat = Chat.get(owner=self.chat_owner_username)
                chat.decrement_support_active()
                chat.save()
                return not chat.is_support_active

            if await database_sync_to_async(supporter_leave_chat)():
                await self.group_send_json(self.group, self.chat_lost_support)
                await self.group_send_json(SupportConsumer.GROUP, SupportConsumer.chat_lost_support,
                                           self.chat_owner_username)

    async def send_chat_message_dict(self, chat_message: dict):
        await self.send_json({
            'type': 'message',
            **self._inflate_chat_message_dict(chat_message)
        })

    def _inflate_chat_message_dict(self, chat_message: dict) -> dict:
        return {
            'id': chat_message['id'],
            'message': chat_message['message'],
            'isOwnMessage': self.user.username == chat_message['username'],
            'userRealName': chat_message['userRealName'],
            'timestamp': chat_message['timestamp']
        }


@websocket_consumer_decorator
class SupportConsumer(SupporterStatusListener, WebsocketConsumer):
    GROUP = 'support'

    def __init__(self, scope: dict, user: User):
        super().__init__(scope, user)
        self.is_active = None
        self.groups = [self.GROUP, SupporterStatusListener.GROUP]

    @WebsocketConsumer.login_required(accept=True, close=True)
    @WebsocketConsumer.staff_required(close=True)
    async def connect(self):
        self.is_active = (await database_sync_to_async(Supporter.get_or_create)(user=self.user)).is_active

    @WebsocketConsumer.if_opened(raise_exception=True)
    async def receive_json(self, content: dict, **kwargs):  # noqa: C901
        if 'type' not in content:
            await self.send_json(Error.MALFORMED_REQUEST)
            return

        content_type = content['type']
        if content_type == 'getChats':
            await self.command_get_chats()
        elif content_type == 'getStatus':
            await self.command_get_status()
        elif content_type == 'setStatus':
            if 'status' not in content or not isinstance(content['status'], str):
                await self.send_json(Error.MALFORMED_REQUEST)
                return
            await self.command_set_status(content['status'])
        else:
            await self.send_json(Error.UNKNOWN_REQUEST)

    async def command_get_chats(self):
        @transaction.atomic
        def get_chats() -> List[Tuple[Chat, ChatMessage]]:
            return list((_chat, ChatMessage.objects.filter(chat=_chat).order_by('-timestamp').first())
                        for _chat in Chat.objects.prefetch_related('id').all())

        for chat, latest_chat_message in await database_sync_to_async(get_chats)():
            await self.send_chat(chat.owner.username, user_get_real_name(chat.owner),
                                 chat.unread_owner_messages, chat.is_support_active,
                                 latest_chat_message.message,
                                 _datetime_to_epoch_millis(latest_chat_message.timestamp))

    async def command_get_status(self):
        await self.supporter_status_changed(self.is_active, self.user.username, True)

    async def command_set_status(self, status: str):
        if status not in ['active', 'inactive']:
            await self.send_json(Error.MALFORMED_REQUEST)
            return

        await self.set_active(status == 'active')

    async def send_chat(self, chat_owner: str, chat_owner_real_name: str, unread_messages: int, is_support_active: bool,
                        message: str, timestamp: int):
        await self.send_json({
            'type': 'chat',
            'chatOwner': chat_owner,
            'chatOwnerRealName': chat_owner_real_name,
            'unreadMessages': unread_messages,
            'isSupportActive': is_support_active,
            'message': message,
            'timestamp': timestamp
        })

    async def new_message(self, chat_owner: str, message: str, timestamp: int):
        await self.send_json({
            'type': 'newMessage',
            'chatOwner': chat_owner,
            'message': message,
            'timestamp': timestamp
        })

    async def unread_messages_count(self, chat_owner: str):
        unread_messages = 0
        chat = await database_sync_to_async(Chat.get)(owner=chat_owner)
        if chat is not None:
            unread_messages = chat.unread_owner_messages
        await self.send_json({
            'type': 'unreadMessagesCount',
            'chatOwner': chat_owner,
            'unreadMessagesCount': unread_messages
        })

    async def chat_gained_support(self, chat_owner: str):
        await self.send_json({
            'type': 'chatGainedSupport',
            'chatOwner': chat_owner
        })

    async def chat_lost_support(self, chat_owner: str):
        await self.send_json({
            'type': 'chatLostSupport',
            'chatOwner': chat_owner
        })

    @WebsocketConsumer.if_opened(raise_exception=True)
    async def disconnect(self, code):
        pass

    async def set_active(self, active: bool):
        if self.is_active != active:
            self.is_active = active

            @transaction.atomic
            def update() -> Optional[bool]:
                old_has_active = Supporter.has_active()
                supporter = Supporter.get_or_create(self.user)
                supporter.is_active = active
                supporter.save()
                has_active = Supporter.has_active()
                if old_has_active != has_active:
                    return has_active
                return None

            support_online = await database_sync_to_async(update)()
            if support_online is not None:
                await self.group_send_json(SupportStatusListener.GROUP, SupportStatusListener.support_status_changed,
                                           support_online)

            await self.supporter_status_changed(self.is_active, self.user.username, True)

            await self.group_send_json(SupporterStatusListener.GROUP, SupporterStatusListener.supporter_status_changed,
                                       self.is_active, self.user.username)

    async def supporter_status_changed(self, is_active: bool, username: str, force: bool = False):
        if username == self.user.username:
            if force or self.is_active != is_active:
                self.is_active = is_active
                await self.send_json({
                    'type': 'status',
                    'status': 'active' if self.is_active else 'inactive',
                })


def _datetime_to_epoch_millis(timestamp: datetime) -> int:
    return int(timestamp.timestamp() * 1000)


def _chat_message_to_dict(chat_message: ChatMessage):
    return {
        'id': chat_message.id,
        'message': chat_message.message,
        'username': chat_message.user.username,
        'userRealName': user_get_real_name(chat_message.user),
        'timestamp': _datetime_to_epoch_millis(chat_message.timestamp)
    }
