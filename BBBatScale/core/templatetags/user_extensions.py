from django import template

from core.models import User

register = template.Library()


@register.simple_tag(name='real_name')
def user_get_real_name(user: User) -> str:
    return f'{user.last_name}, {user.first_name}'
