from django import template
from django.conf import settings

register = template.Library()


@register.filter(name='in_group')
def in_group(user, group_name):
    if user.is_superuser:
        return True
    return user.groups.filter(name=group_name).exists()


@register.filter(name='in_moderators_group')
def in_moderators_group(user):
    return in_group(user, settings.MODERATORS_GROUP)
