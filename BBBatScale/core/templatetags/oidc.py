from django import template
from django.conf import settings

register = template.Library()


@register.simple_tag(name="IS_OIDC_ENABLED")
def IS_OIDC_ENABLED():
    return settings.OIDC_ENABLED
