from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.urls import path

from core import views

urlpatterns = [
    path('api/callback/bbb/', views.callback_bbb, name="callback_bbb"),
    path('api/recording/postporocessing/done/', views.recording_callback),
    path('api/servers/registration/', views.api_server_registration, name="api_server_registration"),
    path('servers/overview', views.servers_overview, name="servers_overview"),
    path('rooms/overview', views.rooms_overview, name="rooms_overview"),
    path('tenants/overview', views.tenant_overview, name="tenants_overview"),
    path('rooms/configs/overview', views.room_config_overview, name="room_configs_overview"),

    path('server/create/', login_required(views.server_create), name="server_create"),
    path('server/delete/<int:server>', login_required(views.server_delete), name="server_delete"),
    path('server/update/<int:server>', login_required(views.server_update), name="server_update"),

    path('room/create/', login_required(views.room_create), name="room_create"),
    path('room/delete/<int:room>', login_required(views.room_delete), name="room_delete"),
    path('room/update/<int:room>', login_required(views.room_update), name="room_update"),

    path('home_room/update<int:home_room>', login_required(views.home_room_update), name="home_room_update"),

    path('personal_room/create', login_required(views.personal_room_create), name="personal_room_create"),
    path('personal_room/update/<int:personal_room>', login_required(views.personal_room_update),
         name="personal_room_update"),
    path('personal_room/delete/<int:personal_room>', login_required(views.personal_room_delete),
         name="personal_room_delete"),

    path('statistics', views.statistics, name="statistics"),

    path('importexport', login_required(views.import_export), name="import_export"),
    path('export/json', login_required(views.export_download_json), name="export_download_json"),
    path('export/csv', login_required(views.export_download_csv), name="export_download_csv"),
    path('import/json', login_required(views.import_upload_json), name="import_upload_json"),
    path('import/csv', login_required(views.import_upload_csv), name="import_upload_csv"),

    path('room/config/create/', login_required(views.room_config_create), name="room_config_create"),
    path('room/config/delete/<int:room_config>', login_required(views.room_config_delete), name="room_config_delete"),
    path('room/config/update/<int:room_config>', login_required(views.room_config_update), name="room_config_update"),

    path('tenant/create/', login_required(views.tenant_create), name="tenant_create"),
    path('tenant/delete/<int:tenant>', login_required(views.tenant_delete), name="tenant_delete"),
    path('tenant/update/<int:tenant>', login_required(views.tenant_update), name="tenant_update"),

    path('recordings/overview/', login_required(views.recordings_list), name="recordings_list"),

    path('users/overview/', login_required(views.users_overview), name="users_overview"),
    path('user/create/', login_required(views.user_create), name="user_create"),
    path('user/change/password/', login_required(views.change_password), name="change_password"),
    path('user/change/theme/', views.change_theme, name="change_theme"),
    path('user/delete/<int:user>', login_required(views.user_delete), name="user_delete"),
    path('user/update/<int:user>', login_required(views.user_update), name="user_update"),

    path('join/create/meeting/<str:room>', views.join_or_create_meeting, name="join_or_create_meeting"),
    path('create/meeting/<str:room>', views.create_meeting, name="create_meeting"),
    path('join/meeting/<str:room>', views.join_meeting, name="join_meeting"),
    path('join/meeting/anon/', views.session_set_username, name="session_set_username"),
    path('meeting/status/<str:meeting_id>', views.get_meeting_status, name="get_meeting_status"),
    path('room/meeting/end/<int:room_pk>', login_required(views.force_end_meeting), name="force_end_meeting"),

    path('<str:tenant_name>/bigbluebutton/api/', views.bbb_initialization_request),
    path('<str:tenant_name>/bigbluebutton/api/join', views.bbb_api_join),
    path('<str:tenant_name>/bigbluebutton/api/create', views.bbb_api_create),
    path('<str:tenant_name>/bigbluebutton/api/getMeetingInfo', views.bbb_api_get_meeting_info),
    path('<str:tenant_name>/bigbluebutton/api/end', views.bbb_api_end),
    path('<str:tenant_name>/bigbluebutton/api/getRecordings', views.bbb_api_get_recordings),

    path('settings', views.settings_edit, name="settings_edit"),
    path('coowner/user/search', views.json_search_user, name="json_search_user")
]

if settings.SUPPORT_CHAT_ENABLED:
    urlpatterns.append(path('support', login_required(views.support_chat_staff_overview),
                            name='support_chat_staff_overview'))
