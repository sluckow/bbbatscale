# encoding: utf-8
import logging
from django.core.management.base import BaseCommand
from tendo.singleton import SingleInstance, SingleInstanceException

from core.models import Room, Server, GeneralParameter
from django.db import transaction

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    help = 'perform health check on all servers'

    @transaction.non_atomic_requests
    def handle(self, *args, **options):

        try:
            SingleInstance()
            # perform health check and data collection on all known server
            logger.info("Start checking servers stats")
            print('Performing health checks...')
            for server in Server.objects.all():
                server.collect_stats()
            logger.info("Done checking servers stats")

            logger.info("Start updating max counter.")
            # update max counter
            gp = GeneralParameter.load()
            participant_current = Room.get_participants_current()
            if participant_current > gp.participant_total_max:
                gp.participant_total_max = participant_current
                gp.save()
            logger.info("Done updating max counter.")

        except SingleInstanceException as e:
            logger.error(e)
