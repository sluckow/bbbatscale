import logging
from csv import DictReader
from hashlib import sha1
from urllib.parse import urlencode
from django.http import HttpResponse
import requests
import xmltodict

logger = logging.getLogger(__name__)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class BigBlueButton:
    def __init__(self, bbb_server_url, bbb_server_salt):
        if not bbb_server_url.startswith("https://"):
            if bbb_server_url.startswith("http://"):
                bbb_server_url = bbb_server_url[:4] + "s" + bbb_server_url[4:]
            else:
                bbb_server_url = "https://{}".format(bbb_server_url)
        if not bbb_server_url.endswith("/bigbluebutton/api/"):
            bbb_server_url = "{}/bigbluebutton/api/".format(bbb_server_url)

        self.__url = bbb_server_url
        self.__salt = bbb_server_salt

    def get_api_call_url(self, api_method, api_params):
        checksum = self.create_salt(api_method, api_params)
        if api_params:
            api_params = "{}&".format(api_params)
        return "{}{}?{}checksum={}".format(self.__url, api_method, api_params, checksum)

    # BBB API GET Meetings
    def get_meetings(self):
        logger.info("Start BBB API get Meetings.")
        return requests.get(self.get_api_call_url("getMeetings", ""), timeout=2)

    @staticmethod
    def validate_get_meetings(response):
        meetings = []
        data = xmltodict.parse(response.text)
        if data['response']['returncode'] != "SUCCESS":
            raise Exception('API Call failed')

        try:
            if data['response']['meetings'] is not None:
                if type(data['response']['meetings']['meeting']) == list:
                    meetings = data['response']['meetings']['meeting']
                else:
                    meetings.append(data['response']['meetings']['meeting'])
        except Exception as e:
            logger.error("Error while make api call getMeetings.")
            logger.error(e)
            return None

        logger.info("End BBB API get Meetings.")
        return meetings

    # BBB API Create
    def create(self, params):
        api_call = self.get_api_call_url("create", self.create_params(params))
        return requests.get(api_call, timeout=2)

    @staticmethod
    def validate_create(response):
        data = xmltodict.parse(response.text)
        if data['response']['returncode'] != "SUCCESS":
            return False
        return True

    # BBB API Join
    def join(self, params):
        return self.get_api_call_url("join", self.create_params(params))

    # BBB API End
    def end(self, meeting_id, pw):
        api_call = self.get_api_call_url("end", self.create_params({"meetingID": meeting_id, "password": pw}))
        return requests.get(api_call, timeout=2)

    @staticmethod
    def validate_end(response):
        data = xmltodict.parse(response.text)
        if data['response']['returncode'] == "SUCCESS":
            return True
        return False

    # BBB Is Meeting Running
    def is_meeting_running(self, meeting_id):
        api_call = self.get_api_call_url("isMeetingRunning", self.create_params({"meetingID": meeting_id}))
        return requests.get(api_call, timeout=2)

    @staticmethod
    def validate_is_meeting_running(response):
        data = xmltodict.parse(response.text)
        if data['response']['running'] == "true":
            return True
        return False

    # BBB Get Meeting Infos
    def get_meeting_infos(self, meeting_id):
        api_call = self.get_api_call_url("getMeetingInfo", self.create_params({"meetingID": meeting_id}))
        return requests.get(api_call, timeout=2)

    @staticmethod
    def validate_get_meeting_infos(response):
        data = xmltodict.parse(response.text)
        return data

    # BBB Create Web Hook
    def create_web_hook(self, params):
        _params = self.create_params(params)
        checksum = self.create_salt("hooks/create", _params)
        url = "{}hooks/create?{}&checksum={}".format(self.__url, _params, checksum)
        return requests.get(url, timeout=2)

    @staticmethod
    def validate_create_web_hook(response):
        data = xmltodict.parse(response.text)
        try:
            if data['response']['returncode'] != "SUCCESS":
                return False
            return True
        except KeyError:
            logger.error("{} did not respone with a valid XML for webhook creation.")
            return False

    # BBB GET Recordings
    def get_recordings(self, params):
        api_call = self.get_api_call_url("getRecordings", self.create_params(params))
        return requests.get(api_call, timeout=10)

    @staticmethod
    def validate_get_recordings(response):
        data = xmltodict.parse(response.text)
        return data

    def create_salt(self, api_method, api_params):
        return sha1("{}{}{}".format(api_method, api_params, self.__salt).encode("utf-8")).hexdigest()

    @staticmethod
    def create_params(params):
        return urlencode(params)

    @property
    def url(self):
        return self.__url


def validate_csv(reader: DictReader):
    # validates uploaded csv file before importing it to db
    val_room_name = []

    room_dup = []

    for row in reader:
        assert 'name' in row
        assert 'is_public' in row
        assert 'comment_public' in row
        assert 'comment_private' in row

        if row['name'] not in val_room_name:
            val_room_name.append(row['name'])
        elif row['name'] not in room_dup:
            room_dup.append(row['name'])

    return [set(room_dup)]


def validate_bbb_api_call(api_method, params, tenant):
    checksum = params.pop('checksum')
    _checksum = sha1(
        "{}{}{}".format(api_method, BigBlueButton.create_params(params), tenant.token_registration).encode(
            "utf-8")).hexdigest()
    return True if checksum[0] == _checksum else False


def bbb_checksum_error_xml_response():
    return HttpResponse("""
        <response>
            <returncode>FAILED</returncode>
            <messageKey>checksumError</messageKey>
            <message>You did not pass the checksum security check</message>
        </response>
    """, content_type='text/xml')


def bbb_meeting_not_found():
    return HttpResponse("""
        <response>
            <returncode>FAILED</returncode>
            <messageKey>notFound</messageKey>
            <message>We could not find a meeting with that meeting ID</message>
        </response>
    """, content_type='text/xml')


def bbb_no_recordings_meeting():
    return HttpResponse("""
        <response>
            <returncode>SUCCESS</returncode>
            <recordings/>
            <messageKey>noRecordings</messageKey>
            <message>There are no recordings for the meeting(s).</message>
        </response>
    """, content_type='text/xml')


def set_room_config(room):
    room.mute_on_start = room.config.mute_on_start
    room.all_moderator = room.config.all_moderator
    room.everyone_can_start = room.config.everyone_can_start
    room.authenticated_user_can_start = room.config.authenticated_user_can_start
    room.guest_policy = room.config.guest_policy
    room.allow_guest_entry = room.config.allow_guest_entry
    room.access_code = room.config.access_code
    room.access_code_guests = room.config.access_code_guests
    room.disable_cam = room.config.disable_cam
    room.disable_mic = room.config.disable_mic
    room.allow_recording = room.config.allow_recording
    room.url = room.config.url
    room.welcome_message = room.config.welcome_message
    room.logoutUrl = room.config.logoutUrl
    room.dialNumber = room.config.dialNumber
    return room
