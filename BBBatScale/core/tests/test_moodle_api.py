from django.test import TestCase
from core.models import Tenant, Server
from core.constants import SERVER_STATE_UP
import urllib
from core.utils import validate_bbb_api_call


class MoodleAPITests(TestCase):

    def setUp(self) -> None:
        self.fbi = Tenant.objects.create(
            name="FBI",
            token_registration="tIKUDXeAbhtIKUDXeAbhtIKUDXeAbh",
        )
        self.server1 = Server.objects.create(
            tenant=self.fbi,
            dns="bbb49.test.test.de",
            shared_secret="SERVER1",
            state=SERVER_STATE_UP,
        )

    def test_validate_bbb_api_call(self):
        params = urllib.parse.parse_qs(
            "&allowStartStopRecording=true&attendeePW=ap&autoStartRecording=false&meetingID=random-7391472" +
            "&moderatorPW=mp&name=random-7391472&record=false&voiceBridge=78278&welcome=%3Cbr%3EWelcome+to+%" +
            "3Cb%3E%25%25CONFNAME%25%25%3C%2Fb%3E%21&checksum=ed9a2d92582791a62104e4ee5e3f5211c5ebe3df")
        # self.assertTrue(validate_bbb_api_call("create", params, self.fbi))
        params = urllib.parse.parse_qs(
            "allowStartStopRecording=true&attendeePW=ap&autoStartRecording=false&meetingID=random-7391472" +
            "&moderatorPW=mp&name=random-7391472&record=false&voiceBridge=78278&welcome=%3Cbr%3EWelcome+to+%" +
            "3Cb%3E%25%25CONFNAME%25%25%3C%2Fb%3E%21&checksum=safadgadasfa")
        self.assertFalse(validate_bbb_api_call("create", params, self.fbi))

    def test_moodle_initial_request_validation(self):
        params = urllib.parse.parse_qs("&checksum=17ee86f6d61115bb04222e515843c2b0bc5095de")
        print(params)
        self.assertTrue(validate_bbb_api_call("", params, self.fbi))
