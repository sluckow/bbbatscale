import os
from datetime import datetime, timezone

from django.conf import settings
from django.test import TestCase
from freezegun import freeze_time

from core.models import Tenant, Server, Room, RoomEvent, MoodleRoom, Meeting, User, get_default_room_config
from core.services import collect_room_occupancy, get_rooms_with_current_next_event, \
    moodle_get_or_create_room_with_params, get_join_password


class CollectRoomOccupancy(TestCase):

    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="FBI",
        )

        self.hda = Tenant.objects.create(
            name="h_da",
        )

        self.bbb_server = Server.objects.create(
            tenant=self.fbi,
            dns="example.org",
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_d14_0303_qis = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server,
            name="D14/03.03",
            participant_count=10,
            videostream_count=5,
            event_collection_strategy="QISICalCollector",
            event_collection_parameters='{"qis_url": "qis.example.org", '
                                        '"qis_id": "118", '
                                        '"qis_encoding": "UTF-8"}',
            config=get_default_room_config()
        )

        file_path = "file://" + os.path.dirname(
            os.path.realpath(__file__)) + "/test_data/SimpleICalCollector_testdata.ics"
        self.room_d14_0204_ical = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server,
            name="D14/02.04",
            participant_count=10,
            videostream_count=5,
            event_collection_strategy="SimpleICalCollector",
            event_collection_parameters='{"iCal_url": "' + file_path + '", "iCal_encoding": "UTF-8"}',
            config=get_default_room_config()
        )

        settings.EVENT_COLLECTION_SYNC_SYNC_HOURS = 24

    @freeze_time("2020-05-25 06:30:00", tz_offset=0)
    def test_collect_room_occupancy(self):
        collect_room_occupancy(self.room_d14_0204_ical.pk)

        room_events = RoomEvent.objects.filter(room=self.room_d14_0204_ical)
        self.assertEqual(len(room_events), 4)


class RoomEventsNextCurrentTest(TestCase):
    def setUp(self) -> None:
        self.fbi = Tenant.objects.create(
            name="FBI",
        )
        self.room_d14_0204 = Room.objects.create(
            tenant=self.fbi,
            name="D14/02.04",
            config=get_default_room_config()
        )
        self.room_d14_0204_event_one = RoomEvent.objects.create(
            uid="Crypto",
            room=self.room_d14_0204,
            name="Crypto",
            start=datetime(2020, 5, 25, 8, 30, tzinfo=timezone.utc),
            end=datetime(2020, 5, 25, 10, 00, tzinfo=timezone.utc)
        )

        self.room_d14_0204_event_two = RoomEvent.objects.create(
            uid="DB2",
            room=self.room_d14_0204,
            name="DB2",
            start=datetime(2020, 5, 25, 10, 15, tzinfo=timezone.utc),
            end=datetime(2020, 5, 25, 11, 45, tzinfo=timezone.utc)
        )
        self.room_d14_0204_event_three = RoomEvent.objects.create(
            uid="OOAD",
            room=self.room_d14_0204,
            name="OOAD",
            start=datetime(2020, 5, 25, 14, 15, tzinfo=timezone.utc),
            end=datetime(2020, 5, 25, 15, 45, tzinfo=timezone.utc)
        )

    @freeze_time("2020-05-25 06:30:00", tz_offset=0)
    def test_get_rooms_with_current_next_event_without_current_with_next(self):
        rooms = get_rooms_with_current_next_event()
        self.assertIsNone(rooms[0].room_occupancy_current)
        self.assertEqual(rooms[0].room_occupancy_next, "Crypto")

    @freeze_time("2020-05-25 08:30:00", tz_offset=0)
    def test_get_rooms_with_current_next_event_with_current_and_next(self):
        rooms = get_rooms_with_current_next_event()
        self.assertEqual(rooms[0].room_occupancy_current, "Crypto")
        self.assertEqual(rooms[0].room_occupancy_next, "DB2")

    @freeze_time("2020-05-25 15:30:00", tz_offset=0)
    def test_get_rooms_with_current_next_event_with_current_and_without_next(self):
        rooms = get_rooms_with_current_next_event()
        self.assertEqual(rooms[0].room_occupancy_current, "OOAD")
        self.assertIsNone(rooms[0].room_occupancy_next)

    @freeze_time("2020-05-25 16:30:00", tz_offset=0)
    def test_get_rooms_with_current_next_event_without_current_and_next(self):
        rooms = get_rooms_with_current_next_event()
        self.assertIsNone(rooms[0].room_occupancy_current)
        self.assertIsNone(rooms[0].room_occupancy_next)


class GetOrCreateMoodleRoomTest(TestCase):

    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="FBI",
        )

        self.bbb_server = Server.objects.create(
            tenant=self.fbi,
            dns="bbb10.fbi.h-da.de",
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.moodle_room_params = {
            'name': 'test_moodle_room_name',
            'meetingID': "123-456-789",
            'attendeePW': 'test_attendee_password',
            'moderatorPW': "test_moderator_password",
        }

    def test_moodle_get_room_with_params(self):
        existing_moodle_room = moodle_get_or_create_room_with_params(self.moodle_room_params, self.fbi)
        moodle_room = MoodleRoom.objects.get(meeting_id="123-456-789")
        self.assertEqual(existing_moodle_room[1], True)
        self.assertEqual(existing_moodle_room[0], moodle_room)
        self.moodle_room_params['meetingID'] = "8465132123468432"
        existing_moodle_room = moodle_get_or_create_room_with_params(self.moodle_room_params, self.fbi)
        self.assertEqual(MoodleRoom.objects.all().count(), 2)
        self.assertNotEqual(existing_moodle_room[0].name, moodle_room.name)


class JoinPasswordTest(TestCase):

    @freeze_time("2020-05-25 16:30:00", tz_offset=0)
    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="FBI",
        )

        self.hda = Tenant.objects.create(
            name="h_da",
        )

        self.bbb_server = Server.objects.create(
            tenant=self.fbi,
            dns="bbb10.fbi.h-da.de",
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.room_d14_0303 = Room.objects.create(
            tenant=self.fbi,
            server=self.bbb_server,
            name="D14/03.03",
            attendee_pw="test_attendee_password",
            moderator_pw="test_moderator_password",
            all_moderator=False,
            config=get_default_room_config()
        )

        self.sample_meeting_room = Meeting.objects.create(
            room_name=self.room_d14_0303.name,
            meeting_id="12345678",
            creator="sample_meeting_creator",
            replay_id="87654321",
        )

        self.test_user = User.objects.create(
            id=None,
            pk=None,
            username="test_username",
            is_staff=False,
            is_active=False,
            is_superuser=False,
        )

    def test_get_join_password(self):
        self.assertEqual("test_attendee_password",
                         get_join_password(self.test_user, self.room_d14_0303, self.sample_meeting_room))
