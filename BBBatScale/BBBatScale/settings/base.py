import os

from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import gettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = os.environ['DJANGO_SECRET_KEY']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['*']
BASE_URL = os.environ['BASE_URL']


RECORDINGS_SECRET = os.environ['RECORDINGS_SECRET']

OIDC_ENABLED = os.environ.get('OIDC_ENABLED', '').lower() == 'true'
if OIDC_ENABLED:
    # Mandatory settings for oidc
    OIDC_RP_CLIENT_ID = os.environ['OIDC_RP_CLIENT_ID']
    OIDC_RP_CLIENT_SECRET = os.environ['OIDC_RP_CLIENT_SECRET']
    OIDC_OP_AUTHORIZATION_ENDPOINT = os.environ['OIDC_OP_AUTHORIZATION_ENDPOINT']
    OIDC_OP_TOKEN_ENDPOINT = os.environ['OIDC_OP_TOKEN_ENDPOINT']
    OIDC_OP_USER_ENDPOINT = os.environ['OIDC_OP_USER_ENDPOINT']
    OIDC_OP_JWKS_ENDPOINT = os.environ['OIDC_OP_JWKS_ENDPOINT']
    OIDC_OP_END_SESSION_ENDPOINT = os.environ['OIDC_OP_END_SESSION_ENDPOINT']
    # Optional settings for oidc
    OIDC_RP_SCOPES = os.environ.get('OIDC_RP_SCOPES', 'openid profile email')
    OIDC_VERIFY_SSL = os.environ.get('OIDC_VERIFY_SSL', 'True').lower() == 'true'
    OIDC_RP_SIGN_ALGO = 'RS256'
    # Method used to log out the user
    OIDC_OP_LOGOUT_URL_METHOD = 'BBBatScale.auth.provider_logout'

OIDC_REFRESH_ENABLED = os.environ.get('OIDC_REFRESH_ENABLED', 'True').lower() == 'true'
if OIDC_ENABLED and OIDC_REFRESH_ENABLED:
    OIDC_RENEW_ID_TOKEN_EXPIRY_SECONDS = int(os.environ.get('OIDC_RENEW_ID_TOKEN_EXPIRY_SECONDS', '900'))

if OIDC_ENABLED:
    LOGIN_URL = '/oidc/authenticate/'
    LOGOUT_URL = '/oidc/logout/'
else:
    LOGIN_URL = '/login/'
    LOGOUT_URL = '/logout/'

LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'

# Application definition

SUPPORT_CHAT_ENABLED = os.environ.get('SUPPORT_CHAT', '').lower() == 'enabled'

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'core',

    'crispy_forms',
]

if OIDC_ENABLED:
    INSTALLED_APPS.append('mozilla_django_oidc')

if SUPPORT_CHAT_ENABLED:
    INSTALLED_APPS.append('support_chat')
    INSTALLED_APPS.append('channels')

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'whitenoise.middleware.WhiteNoiseMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

if OIDC_ENABLED and OIDC_REFRESH_ENABLED:
    MIDDLEWARE.append('mozilla_django_oidc.middleware.SessionRefresh')

ROOT_URLCONF = 'BBBatScale.urls'

WSGI_APPLICATION = 'BBBatScale.wsgi.application'
ASGI_APPLICATION = 'BBBatScale.routing.application'

# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases


# Password validation
# https://docs.djangoproject.com/en/3.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/3.0/topics/i18n/

LANGUAGE_CODE = 'en'

LANGUAGES = (
    ('en', _('English')),
    ('de', _('German')),
    ('it', _('Italian')),
    ('es', _('Spanish')),
    ('gl', _('Galician')),
)

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
                'BBBatScale.context_processors.export_vars',
            ],
        },
    },
]

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')

STATIC_URL = '/static/'

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

CRISPY_TEMPLATE_PACK = 'bootstrap4'

EVENT_COLLECTION_SYNC_SYNC_HOURS = 24

LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
    os.path.join(BASE_DIR, 'support_chat/locale'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ['POSTGRES_DB'],
        'USER': os.environ['POSTGRES_USER'],
        'PASSWORD': os.environ['POSTGRES_PASSWORD'],
        'HOST': os.environ['POSTGRES_HOST'],
        'PORT': int(os.environ.setdefault('POSTGRES_PORT', '5432')),
        'ATOMIC_REQUESTS': True,
    },
}

if SUPPORT_CHAT_ENABLED:
    CHANNEL_LAYERS = {
        'default': {
            'BACKEND': 'channels_redis.core.RedisChannelLayer',
            'CONFIG': {
                'hosts': [
                    {
                        'address': (os.environ['REDIS_HOST'], int(os.environ.setdefault('REDIS_PORT', '6379'))),
                        'db': int(os.environ.setdefault('REDIS_DATABASE', '0')),
                        'password': os.getenv('REDIS_PASSWORD', None),
                    },
                ],
            },
        },
    }

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '{levelname} {asctime} {module} {funcName} {lineno} {process:d} {thread:d} {message}',
            'style': '{',
        },
        'simple': {
            'format': '{levelname} {message}',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        }
    },
    'root': {
        'handlers': ['console'],
        'level': 'INFO',
    },
}

AUTH_USER_MODEL = 'core.User'

MODERATORS_GROUP = os.getenv('BBBATSCALE_MODERATORS_GROUP', 'Teacher')

AUTHENTICATION_BACKENDS = [
    'django.contrib.auth.backends.ModelBackend',
]

if OIDC_ENABLED:
    AUTHENTICATION_BACKENDS.append('BBBatScale.auth.openIdBackend')

try:
    from .ldap_config import *  # noqa: F401,F403

    HAVE_LDAP_CONFIG = True
except ImportError:
    HAVE_LDAP_CONFIG = False

if HAVE_LDAP_CONFIG:
    try:
        # Importing for side effects here so silencing the not-entirely-helpful
        # F401s.
        import ldap  # noqa: F401
        import django_auth_ldap  # noqa: F401
    except ImportError:
        raise ImproperlyConfigured('LDAP auth configured but django-auth-ldap missing.')

    AUTHENTICATION_BACKENDS.insert(0, 'django_auth_ldap.backend.LDAPBackend')
