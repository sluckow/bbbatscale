# Migration Path from 1.X to 2.0

In the 2.0 release a major database change occurs.
The introduction of a custom user model.
This will break your installation, if not handeled correctly.

To mitigate this the following steps are suggested and should work.

1. Update from your current version to the latest 1.1.4 version.
2. Export your database through import/export page with the JSON export.
3. Open your JSON export, remove all rooms with no config set and rename instances to servers.
SCREENSHOT
4. Stop BBB@Scale.
5. Drop your current database.
6. Upgrade your application to the latest Version (2.X.X)
7. Start BBB@Scale again. If you are using our docker-compose file, the database is automatically created and migrated.
8. Import the database through import/export page with the JSON import. SCREENSHOTS

After you verify your import you are finished the upgrade to BBB@Scale v2.0.0.
Happy working and carry on!
